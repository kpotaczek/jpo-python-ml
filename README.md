
# ML Diabetes Detection in Python

## 1. What has been created?

This repo contains a jupyter notebook file with all steps of data analysis and machine learnign done in python. It covers:

1.  Data preparation
    
2.  Training Machine Learning algorithms (Naive Bayes and Random Forest)
    
3.  Testing Machine Learning algorithms
    
4.  All the charts used for data analysis and algorithms testing
    
5.  Comparison of used algorithms
    

## 2. How to run?

1.  Required libraries
    
    -   Pandas Dataframe
          
    -   scikit-learn
        
    -   matplotlib  All libraries are available by default in anaconda distribution of python
        
2.  Open console (Preferably Anaconda Prompt)
    
3.  CD into this repository directory
    
4.  Run "jupyter notebook" command
    
5.  Inside newly opened web browser tab open **_"diabetes_test.ipynb"_**
    
6.  Run all code
    

## 3. Additional information

Code was written in a way that it is able to run with newest version of anaconda distribution, which can cause some deprecation warnings. Everything should work fine in current python anaconda distribution.

## 4. TODO

-   Document every step and thought processes behind it